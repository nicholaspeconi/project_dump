-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 10, 2018 at 02:52 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `personal_trainer`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `account_id` int(10) NOT NULL,
  `mem_Id` int(10) NOT NULL,
  `card_type` varchar(20) NOT NULL,
  `card_num` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

CREATE TABLE `activity` (
  `activity_Id` int(11) NOT NULL,
  `activity_typecode` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `activity_starttime` time NOT NULL,
  `duration` varchar(10) NOT NULL,
  `emp_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `activitytype`
--

CREATE TABLE `activitytype` (
  `activity_typecode` int(11) NOT NULL,
  `activity_description` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `center`
--

CREATE TABLE `center` (
  `center_Id` int(11) NOT NULL,
  `location` varchar(40) NOT NULL,
  `center_name` varchar(40) NOT NULL,
  `center_managerid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `childcarecheckins`
--

CREATE TABLE `childcarecheckins` (
  `depd_id` int(11) NOT NULL,
  `checkin_datetime` datetime NOT NULL,
  `checkout_datetime` datetime DEFAULT NULL,
  `comments` text NOT NULL,
  `emp_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `dept_id` int(11) NOT NULL,
  `dept_description` varchar(40) NOT NULL,
  `dept_managerid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dependant`
--

CREATE TABLE `dependant` (
  `depd_id` int(10) NOT NULL,
  `depd_firstname` varchar(30) NOT NULL,
  `depd_lastname` varchar(30) NOT NULL,
  `birth_Date` date NOT NULL,
  `guardian_Id` int(10) NOT NULL,
  `medical_concerns` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `emp_id` int(11) NOT NULL,
  `emp_typecode` int(11) NOT NULL,
  `emp_firstname` varchar(40) NOT NULL,
  `emp_lastname` varchar(40) NOT NULL,
  `emp_email` varchar(40) NOT NULL,
  `emp_city` varchar(40) NOT NULL,
  `emp_zip` varchar(7) NOT NULL,
  `emp_street` varchar(40) NOT NULL,
  `emp_phone` int(11) NOT NULL,
  `emp_birthdate` date NOT NULL,
  `emp_startdate` date NOT NULL,
  `emp_status` varchar(30) NOT NULL,
  `center_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employeeauthentication`
--

CREATE TABLE `employeeauthentication` (
  `emp_id` int(11) NOT NULL,
  `emp_username` varchar(50) NOT NULL,
  `emp_password` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employeetype`
--

CREATE TABLE `employeetype` (
  `emp_typecode` int(11) NOT NULL,
  `emp_description` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `item_id` int(11) NOT NULL,
  `item_cost` float NOT NULL,
  `item_description` varchar(50) NOT NULL,
  `amount_instock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `mem_id` int(10) NOT NULL,
  `mem_firstname` varchar(30) NOT NULL,
  `mem_lastname` varchar(30) NOT NULL,
  `mem_email` varchar(30) DEFAULT NULL,
  `mem_street` varchar(60) NOT NULL,
  `mem_zip` varchar(7) NOT NULL,
  `mem_phone` int(11) NOT NULL,
  `mem_birthdate` date NOT NULL,
  `mem_city` varchar(40) NOT NULL,
  `membership_Id` int(10) NOT NULL,
  `spouse` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `memberactivity`
--

CREATE TABLE `memberactivity` (
  `membership_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `depd_name` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `memberauthentication`
--

CREATE TABLE `memberauthentication` (
  `mem_id` int(11) NOT NULL,
  `mem_username` varchar(50) NOT NULL,
  `mem_password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `membership`
--

CREATE TABLE `membership` (
  `membership_id` int(10) NOT NULL,
  `membership_typecode` int(2) NOT NULL,
  `membership_startdate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `membertype`
--

CREATE TABLE `membertype` (
  `mem_typecode` int(2) NOT NULL,
  `mem_description` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `salesitem`
--

CREATE TABLE `salesitem` (
  `item_id` int(10) NOT NULL,
  `trans_id` int(10) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `trans_id` int(10) NOT NULL,
  `account_id` int(10) NOT NULL,
  `total_cost` float NOT NULL,
  `trans_datatime` datetime NOT NULL,
  `emp_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`account_id`),
  ADD KEY `memberId` (`mem_Id`);

--
-- Indexes for table `activity`
--
ALTER TABLE `activity`
  ADD PRIMARY KEY (`activity_Id`),
  ADD KEY `activityTypeCode` (`activity_typecode`),
  ADD KEY `employeeId` (`emp_id`);

--
-- Indexes for table `activitytype`
--
ALTER TABLE `activitytype`
  ADD PRIMARY KEY (`activity_typecode`);

--
-- Indexes for table `center`
--
ALTER TABLE `center`
  ADD PRIMARY KEY (`center_Id`),
  ADD KEY `center_managerid` (`center_managerid`);

--
-- Indexes for table `childcarecheckins`
--
ALTER TABLE `childcarecheckins`
  ADD PRIMARY KEY (`depd_id`),
  ADD KEY `emp_id` (`emp_id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`dept_id`),
  ADD KEY `dept_managerid` (`dept_managerid`);

--
-- Indexes for table `dependant`
--
ALTER TABLE `dependant`
  ADD PRIMARY KEY (`depd_id`),
  ADD UNIQUE KEY `guardianId` (`guardian_Id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`emp_id`),
  ADD KEY `employeeTypeCode` (`emp_typecode`),
  ADD KEY `centerId` (`center_id`),
  ADD KEY `departmentId` (`dept_id`);

--
-- Indexes for table `employeetype`
--
ALTER TABLE `employeetype`
  ADD PRIMARY KEY (`emp_typecode`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`mem_id`),
  ADD KEY `spouse` (`spouse`),
  ADD KEY `membershipId` (`membership_Id`);

--
-- Indexes for table `memberactivity`
--
ALTER TABLE `memberactivity`
  ADD KEY `membershipId` (`membership_id`),
  ADD KEY `activityId` (`activity_id`),
  ADD KEY `dependantId` (`depd_name`);

--
-- Indexes for table `membership`
--
ALTER TABLE `membership`
  ADD PRIMARY KEY (`membership_id`),
  ADD KEY `membershipTypeCode` (`membership_typecode`);

--
-- Indexes for table `membertype`
--
ALTER TABLE `membertype`
  ADD PRIMARY KEY (`mem_typecode`);

--
-- Indexes for table `salesitem`
--
ALTER TABLE `salesitem`
  ADD PRIMARY KEY (`item_id`),
  ADD KEY `transactionId` (`trans_id`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`trans_id`),
  ADD KEY `accountId` (`account_id`),
  ADD KEY `employeeId` (`emp_id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `account`
--
ALTER TABLE `account`
  ADD CONSTRAINT `account_ibfk_1` FOREIGN KEY (`mem_Id`) REFERENCES `member` (`mem_id`);

--
-- Constraints for table `activity`
--
ALTER TABLE `activity`
  ADD CONSTRAINT `activity_ibfk_1` FOREIGN KEY (`activity_typecode`) REFERENCES `activitytype` (`activity_typecode`),
  ADD CONSTRAINT `activity_ibfk_2` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`emp_id`);

--
-- Constraints for table `center`
--
ALTER TABLE `center`
  ADD CONSTRAINT `center_ibfk_1` FOREIGN KEY (`center_managerid`) REFERENCES `employee` (`emp_id`);

--
-- Constraints for table `childcarecheckins`
--
ALTER TABLE `childcarecheckins`
  ADD CONSTRAINT `childcarecheckins_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`emp_id`),
  ADD CONSTRAINT `childcarecheckins_ibfk_2` FOREIGN KEY (`depd_id`) REFERENCES `dependant` (`depd_id`);

--
-- Constraints for table `department`
--
ALTER TABLE `department`
  ADD CONSTRAINT `department_ibfk_1` FOREIGN KEY (`dept_managerid`) REFERENCES `employee` (`emp_id`);

--
-- Constraints for table `dependant`
--
ALTER TABLE `dependant`
  ADD CONSTRAINT `dependant_ibfk_1` FOREIGN KEY (`guardian_Id`) REFERENCES `member` (`mem_id`);

--
-- Constraints for table `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`emp_typecode`) REFERENCES `employeetype` (`emp_typecode`),
  ADD CONSTRAINT `employee_ibfk_2` FOREIGN KEY (`center_id`) REFERENCES `center` (`center_Id`),
  ADD CONSTRAINT `employee_ibfk_3` FOREIGN KEY (`dept_id`) REFERENCES `department` (`dept_id`);

--
-- Constraints for table `inventory`
--
ALTER TABLE `inventory`
  ADD CONSTRAINT `inventory_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `salesitem` (`item_id`);

--
-- Constraints for table `member`
--
ALTER TABLE `member`
  ADD CONSTRAINT `member_ibfk_1` FOREIGN KEY (`spouse`) REFERENCES `member` (`mem_id`),
  ADD CONSTRAINT `member_ibfk_2` FOREIGN KEY (`membership_Id`) REFERENCES `membership` (`membership_id`);

--
-- Constraints for table `memberactivity`
--
ALTER TABLE `memberactivity`
  ADD CONSTRAINT `memberactivity_ibfk_1` FOREIGN KEY (`membership_id`) REFERENCES `membership` (`membership_id`),
  ADD CONSTRAINT `memberactivity_ibfk_2` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`activity_Id`),
  ADD CONSTRAINT `memberactivity_ibfk_3` FOREIGN KEY (`depd_name`) REFERENCES `dependant` (`depd_id`);

--
-- Constraints for table `membership`
--
ALTER TABLE `membership`
  ADD CONSTRAINT `membership_ibfk_1` FOREIGN KEY (`membership_typecode`) REFERENCES `membertype` (`mem_typecode`);

--
-- Constraints for table `salesitem`
--
ALTER TABLE `salesitem`
  ADD CONSTRAINT `salesitem_ibfk_1` FOREIGN KEY (`trans_id`) REFERENCES `transaction` (`trans_id`),
  ADD CONSTRAINT `salesitem_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `inventory` (`item_id`);

--
-- Constraints for table `transaction`
--
ALTER TABLE `transaction`
  ADD CONSTRAINT `transaction_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `account` (`account_id`),
  ADD CONSTRAINT `transaction_ibfk_2` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`emp_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
